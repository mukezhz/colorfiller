let color = "white";

const mainBox = document.querySelector("#main-box");
const colorPicker = document.querySelectorAll("input[type='radio']");
// on mouse down event filling the color and adding mouse over event
mainBox.addEventListener("mousedown", (e) => {
  fillColor(e);
  mainBox.addEventListener("mouseover", fillColor);
});
// on mouse up removing the fillcolor from mouse over
mainBox.addEventListener("mouseup", (e) => {
  mainBox.removeEventListener("mouseover", fillColor);
});
// on mouse click removing the fillcolor from mouse over to make sure event is removed
mainBox.addEventListener("click", (e) => {
  mainBox.removeEventListener("mouseover", fillColor);
});

function fillColor(e) {
  e.target.style.backgroundColor = color;
}

colorPicker.forEach((el) => {
  el.addEventListener("click", (e) => {
    console.log(color);
    color = e.target.value;
    console.log(color);
  });
  //   console.log(color.checked);
});

/*
suppose there is 100x100 size box
we can add 1 box whose size is 100x100
if size of box is 50x50 then we can add 4 boxes
    we can obtain by:
    no of box = 100x100 / 100x100 = 1*1 = 1
    no of box = 100x100 / 50x50 = 2*2 = 4
    on our case
    no of box = 500x500 / 50x50 = 10*10 = 100
    here we can say that to create a box of 500x500 from 50x50 we need create 100 box
    */
function createBox(num, width, height) {
  // creating n no of box of size widthxheight
  width = `${width}px`;
  height = `${height}px`;
  for (let i = 0; i < num; i++) {
    const box = document.createElement("div");
    box.style.height = width;
    box.style.width = height;
    box.style.backgroundColor = "blue";
    // box.addEventListener("mousedown", (event) => {
    //   console.log("working");
    // });
    mainBox.appendChild(box);
  }
}
createBox(100, 50, 50);
